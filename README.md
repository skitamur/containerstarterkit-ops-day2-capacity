# Starter Kit : 運用編 キャパシティ管理 ハンズオン

Container Stater Kit で実施するハンズオンの手順を紹介します。  
当ハンズオンによって、OpenShift内のキャパシティ管理を体験することができます。

具体的な手順についてはhandsonフォルダのREADME.mdを参照ください。
---
## [キャパシティ管理ハンズオン手順](./handson/README.md)